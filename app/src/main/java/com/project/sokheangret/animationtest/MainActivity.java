package com.project.sokheangret.animationtest;

import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.ActionMode;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Interpolator;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.imgSva)
    ImageView ivMySva;

    @BindView(R.id.text_view)
    TextView textView;

    ActionMode actionMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

       // Animation animation = AnimationUtils.loadAnimation(this,R.anim.my_animation);

        //ivMySva.setAnimation(animation);

//        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(ivMySva, View.SCALE_X,1f,1.2f, 1.5f);
//        objectAnimator.setRepeatCount(ValueAnimator.INFINITE);
//        objectAnimator.setRepeatMode(ValueAnimator.REVERSE);
//        objectAnimator.start();

        PropertyValuesHolder height = PropertyValuesHolder.ofFloat("height",1f,1.5f);
        PropertyValuesHolder width = PropertyValuesHolder.ofFloat("width",1f,1.5f);
        ValueAnimator valueAnimator = ValueAnimator.ofPropertyValuesHolder(height,width);

        valueAnimator.setRepeatMode(ValueAnimator.REVERSE);
        valueAnimator.setRepeatCount(ValueAnimator.INFINITE);
        valueAnimator.setDuration(1000);
        valueAnimator.setInterpolator(new Interpolator() {
            @Override
            public float getInterpolation(float v) {
                return (float) Math.sin(2 * v * v) + 1.5f;
            }
        });
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float height = (float) valueAnimator.getAnimatedValue("height");
                float width = (float) valueAnimator.getAnimatedValue("width");
                int orHeight = ivMySva.getLayoutParams().height;
                int orWidth = ivMySva.getLayoutParams().width;

//                Toast.makeText(MainActivity.this,orHeight + " : " + orHeight * height, Toast.LENGTH_SHORT).show();
//                ivMySva.setLayoutParams(new Layout).width = orWidth * width;
//                ivMySva.getLayoutParams().height = orHeight * height;

                //ivMySva.setLayoutParams();

                ivMySva.setScaleX(height);
                ivMySva.setScaleY(width);
            }
        });
       // valueAnimator.start();




        textView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
               // if(actionMode != null) return false;
                MyActionMode myActionMode = new MyActionMode();
                actionMode = MainActivity.this.startActionMode(myActionMode, ActionMode.TYPE_FLOATING);
                view.setSelected(true);
                return false;
            }
        });





    }

    void showToast(String sms){
        Toast toast = new Toast(MainActivity.this);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER,0,0);

        View view =getLayoutInflater().inflate(R.layout.toast,null,false);
        ((TextView)view.findViewById(R.id.toast_text)).setText(sms);
        toast.setView(view);
        toast.show();
    }

    class MyActionMode implements ActionMode.Callback{

        @Override
        public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
            actionMode.getMenuInflater().inflate(R.menu.context_menu,menu);

            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
           showToast("Hello My Toast");
            actionMode.finish();
            return true;
        }

        @Override
        public void onDestroyActionMode(ActionMode actionMode) {
            actionMode = null;
        }
    }

}
